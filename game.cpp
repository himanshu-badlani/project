/*
 * Use object-oriented principles to create an autonomous "Snakes & Ladders" game. When the program is run, it should roll
 * a dice and move the player accordingly until the player wins the game.
 *
 * Example output:
 *
 * 1 - Player rolls 6, moves from 1 to 7.
 * 2 - Player rolls 2, moves up the ladder from 9 to 31.
 * ...
 * 9 - Player rolls 1, bitten by snake and moves down from 54 to 34.
 * ...
 * 47 - Player rolls 4, wins the game.
 */

#include<iostream>
#include<stdlib.h>
#include<stdio.h>

using namespace std;

void game();

int main()
{
    int status;

        cout<<"enter 1 to play and 0 to exit:";     //to start the game
        cin>>status;
        switch(status) {
        case 0:
                cout<<"Game End!";
                break;
        case 1:
           game();
        break;
        default:cout<<"Wrong input";
        // missing default (optional)
      }
     // to exit the game; This is a different strategy to end the game. // Complexity increases if you use loops.
    return 0;
}

void game() {
    int dice, i, pos=1;

    // Change this to global variable.
    int snake[5]={17,54,70,87,93}; // positions where the snakes are.
    int ladder[5]={4,28,40,63,75}; // positions where the ladders are.

    while(pos<=100) {

        dice = (rand() % 6) + 1;  //rand() :to generate random number from 1 to 6
        pos=pos+dice;
        cout<<" Player rolls "<<dice<<" "<<endl;
        cout<<" moves to "<<pos;
        for(i=0;i<=5;i++)             // for snake and ladder position check
            {
                if(pos == snake[i])
                {
                    pos=pos-10;
                }
                if(pos == ladder[i])
                {
                    pos=pos+20;
                }

                if ((pos + dice) > 100 ) {
                    continue;  // I expect better strategy
                    }

          }
    }

}

void initialize_game() {
    // snake[5]={17,54,70,87,93}; // positions where the snakes are.
    // adder[5]={4,28,40,63,75}; // positions where the ladders are.
}

/**


if ((pos + dice) > 100 ) {
  continue;  // I expect better strategy
}


if ((pos + dice) == 100) {
    cout << "Player Wins" << '\n';
}

**/

// Player rolls 6, moves from 1 to 7.

// Player only starts when for the first he rolls 6.

//
