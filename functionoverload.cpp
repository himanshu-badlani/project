#include<iostream>
#include<conio.h>
using namespace std;

// Good job! Its always better to specify with comments
int area(int); // square
int area(int,int); //rectangle
float area(float); // Circle
float area(float); //triangle

int main()
{

    cout<<"<<-------Program to Find Area Of different Shapes------>>\n ";
    
    int side, length, breadth; // A single whitespace after comma to enhance readibility
    float radius, base, height;
    
    cout<<"Enter Side of a Square:\n";
    cin>>side;
    
    cout<<"Enter Length and Breadth of Rectangle:\n";
    cin>>length>>breadth;
    
    cout<<"Enter Radius of circle:\n";
    cin>>radius;
    
    cout<<"Enter Base & Height of Triangle:\n";
    cin>>base>>height;
    
    cout<<"Area Of Square is:"<<area(side);
    cout<<"\nArea Of Rectangle is:"<<area(length,breadth);
    cout<<"\nArea of Circle is:"<<area(radius);
    cout<<"\nArea of triangle is "<<area(base,height);
}
    
int area(int side)
{
    return(side*side);
}

int area(int length, int breadth)
{ 
    return(length*breadth);
}

float area(float radius)
{
    return(3.14*radius*radius);
}
    
float area(float base, float height)
{
    return((base*height)/2);
}


